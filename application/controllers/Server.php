<?php
/**
 * Created by PhpStorm.
 * User: myhouse
 * Date: 04/12/17
 * Time: 21:11
 */

class Server extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('session');
    }

    public function server(){

        header('Content-Type: text/event-stream');
        header('Cache-Control: no-cache');

        $data = file_get_contents("chat.txt");

        $trimmed = rtrim($data,"\n");

        $data_array = explode("\n",$trimmed);

        $last_line = end($data_array);

        // If $last_line is not equal to 'last_line' session variable

        if($last_line != $this->session->userdata('last_line')){
            echo "data: $last_line\n\n";
            // Update last line session variable
            $this->session->set_userdata('last_line',$last_line);
        }

        echo "retry: 5000\n";
        ob_flush();
        flush();
    }

    public function chatpost(){
        $text = "";

        $menssagem = $this->input->post('mensagem');

        $usuario = $this->session->userdata('usuario');

        $nome_usuario = $this->input->post('usuario');

        echo $menssagem;

        if(!is_null($menssagem)&&!is_null($usuario)){
            $text = $usuario ." disse: ".$menssagem."\n";

            $handle = fopen("chat.txt", "a");
            fwrite($handle, $text);
            fclose($handle);
            exit();
        }

        if(!is_null($nome_usuario)){
            $this->session->set_userdata('usuario',$nome_usuario);

            echo "sucesso";

            exit;
        }

    }

}