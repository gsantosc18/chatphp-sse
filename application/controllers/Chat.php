<?php
/**
 * Created by PhpStorm.
 * User: myhouse
 * Date: 04/12/17
 * Time: 21:10
 */

class Chat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index(){
        $this->load->view("chat/index");
    }

}