<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Chat</title>
    <base href="<?=base_url()?>">
    <script src="vendor/components/jquery/jquery.js"></script>
    <script src="vendor/twbs/bootstrap/dist/js/bootstrap.js"></script>
    <script src="assets/js/script.js"></script>


    <link rel="stylesheet" href="assets/css/normalize.css">
    <link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap.css">
    <link rel="stylesheet" href="vendor/twbs/bootstrap/dist/css/bootstrap-theme.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>
<div class="container">
    <div class="jumbotron">
        <h1 class="text-center">Bem-vindo ao chat PHP</h1>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">CHAT</div>
        <div class="body clearfix">
            <form class="col-md-12" id="postagem">
                <div id="tela">&nbsp;</div>
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">Mensagem</span>
                        <input type="text" name="mensagem" id="mensagem" class="form-control">
                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-info">Enviar</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>